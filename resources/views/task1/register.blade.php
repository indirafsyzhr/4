<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
    {{ csrf_field() }}
        <!--First Last Name-->
        <label for="first_name">First Name :</label><br><br>
        <input class = "form-control" type="text" name="first_name" value="{{ old('first_name') }}"><br><br>
        <label for="last_name">Last Name :</label><br><br>
        <input class = "form-control" type="text" name="last_name" value="{{ old('last_name') }}"><br><br>
        <!--Gender-->
        <label for="gender">Gender :</label><br><br>
        <input type="radio" id="male" value="male" name="gender">
        <label for="male">Male</label><br>
        <input type="radio" id="female" value="female" name="gender">
        <label for="female">Female</label><br>
        <input type="radio" id="other" value="other" name="gender">
        <label for="other">Other</label><br><br>
        <!--Nationality-->
        <label for="nationality">Nationality :</label><br><br>
        <select>
            <option value="Indonesian">Indonesian</option>
            <option value="American">American</option>
        </select><br><br>
        <!--Language Spoken-->
        <label for="language">Language Spoken :</label><br><br>
        <input type="checkbox" id="bahasa" value="bahasa" name="language">
        <label for="bahasa">Bahasa Indonesia</label><br>
        <input type="checkbox" id="inggris" value="inggris" name="language">
        <label for="inggris">English</label><br>
        <input type="checkbox" id="other_L" value="other_L" name="language">
        <label for="other_L">Other</label><br><br>
        <!--Text Bio-->
        <label for="bio">Bio :</label><br><br>
        <textarea id="bio" rows="6" cols="40"></textarea><br><br>
        <!--Button-->
        <button formaction = '/welcome'>Sign Up</button>
    </form>
</body>
</html>