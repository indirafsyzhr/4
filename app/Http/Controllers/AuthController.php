<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function welcome(Request $request){
        $this->validate($request,[
            'first_name' => 'required',
            'last_name' => 'required'
        ]);
        return view('welcome', ['data' => $request]);
    }
}
